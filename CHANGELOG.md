# Changelog



## v1.2.0 (2021-01-01)






### Administration and Chores
- [release] 1.1.0
- average wage for Q1 2020
- average wage for Q3 2020
- [release] 1.2.0




## v1.1.0 (2020-01-06)


### Bug Fixes
- tax calculation for 2020


### Refactoring
- lock package dependencies




### Administration and Chores
- [release] 1.0.1




## v1.0.1 (2019-07-22)





### Documentation Changes
- usage example


### Administration and Chores
- [release] 1.0.0
- [release] 1.0.1




## v1.0.0 (2019-07-21)

### New Features
- genetic optimization engine
- [CLI] optimize the solution





### Documentation Changes
- [README] CLI usage of --include-taxes


### Administration and Chores
- [release] 0.7.0




## v0.7.0 (2019-07-20)

### New Features
- Payslip






### Administration and Chores
- [release] 0.6.0
- [release] 0.7.0




## v0.6.0 (2019-07-19)






### Administration and Chores
- [release] 0.5.0
- [release] 0.6.0




## v0.5.0 (2019-07-19)

### New Features
- CI
- CLI





### Documentation Changes
- GNU GPLv3 license


### Administration and Chores
- initial commit
- [release] 0.4.1
- [release] 0.5.0




