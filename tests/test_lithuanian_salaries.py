import unittest
import lithuanian_salaries


class LithuanianSalariesTest(unittest.TestCase):
    def test_version(self) -> None:
        self.assertEqual(lithuanian_salaries.__version__, '1.2.0')


if __name__ == '__main__':
    unittest.main()
