import unittest
from random import randint
from lithuanian_salaries import Tax, __minimal_wage__
import sys


class TaxTest(unittest.TestCase):
    def test_negative_salary_exception(self) -> None:
        with self.assertRaises(ValueError):
            Tax(-__minimal_wage__)

    def test_all_taxes_less_than_salary(self) -> None:
        salary = randint(1, sys.maxsize)
        tax = Tax(salary)
        self.assertLess(int(tax), salary)

    def test_max_income_tax_exempt_until_minimal_salary(self) -> None:
        salary = randint(1, __minimal_wage__)
        tax = Tax(salary)
        self.assertEqual(Tax._limit_exempt, tax.exempt)

    def test_less_income_tax_exempt_above_minimal_salary(self) -> None:
        salary = randint(__minimal_wage__ + 1, sys.maxsize)
        tax = Tax(salary)
        self.assertGreater(Tax._limit_exempt, tax.exempt)

    def test_no_income_tax_until_exempt_amount(self) -> None:
        salary = randint(1, Tax._limit_exempt)
        tax = Tax(salary)
        self.assertEqual(0, tax.income_default)
        self.assertEqual(0, tax.income_premium)

    def test_only_default_income_tax_until_premium_threshold(self) -> None:
        salary = randint(__minimal_wage__, Tax._threshold_income_premium - 1)
        tax = Tax(salary)
        self.assertLess(0, tax.income_default)
        self.assertEqual(0, tax.income_premium)

    def test_full_income_tax_after_premium_threshold(self) -> None:
        salary = randint(Tax._threshold_income_premium + 1, sys.maxsize)
        tax = Tax(salary)
        self.assertLess(0, tax.income_default)
        self.assertLess(0, tax.income_premium)

    def test_social_tax_always_applied(self) -> None:
        salary = randint(1, sys.maxsize)
        tax = Tax(salary)
        self.assertLess(0, tax.social_health)
        self.assertLess(0, tax.social_pension)


if __name__ == '__main__':
    unittest.main()
