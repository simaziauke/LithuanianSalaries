import unittest
from random import randint
from lithuanian_salaries import GeneticOptimizer


class GeneticOptimizerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.members = [[randint(1, 10) for _ in range(3)] for _ in range(randint(10, 100))]
        self.fit = lambda x: sum(x)

    def test_push_pop_member(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        member = self.members[0]
        optimizer.push(member)
        self.assertListEqual(member, list(optimizer.pop()))

    def test_population_size(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        for member in self.members:
            optimizer.push(member)
        self.assertEqual(len(self.members), len(optimizer))

    def test_pop_best_known_member(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        members = [[4, 2, 1], [3, 3, 3], [2, 4, 0]]
        for member in members:
            optimizer.push(member)
        self.assertListEqual(members[-1], list(optimizer.pop()))

    def test_pop_sorted_order(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        for member in self.members:
            optimizer.push(member)
        benchmark = 0
        while len(optimizer):
            fitness = self.fit(optimizer.pop())
            self.assertLessEqual(benchmark, fitness)
            benchmark = fitness

    def test_evolve_population_size(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        for member in self.members:
            optimizer.push(member)
        optimizer.evolve()
        self.assertEqual(len(self.members), len(optimizer))

    def test_evolve_pop_as_good_as_known_member(self) -> None:
        optimizer = GeneticOptimizer(self.fit)
        members = [[4, 2, 1], [3, 3, 3], [2, 4, 0]]
        for member in members:
            optimizer.push(member)
        optimizer.evolve()
        self.assertGreaterEqual(self.fit(members[-1]), self.fit(optimizer.pop()))


if __name__ == '__main__':
    unittest.main()
