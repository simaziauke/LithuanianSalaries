import unittest
from lithuanian_salaries import Company


class CompanyTest(unittest.TestCase):
    def setUp(self) -> None:
        self.mean = 1000
        self.std = 300
        self.quartiles = (800, 1100, 2000)
        self.employees = 30

    def test_all_attributes(self) -> None:
        company = Company(self.mean, self.std, self.quartiles, self.employees)
        self.assertEqual(self.mean, company.mean)
        self.assertEqual(self.std, company.std)
        self.assertEqual(self.quartiles, company.quartiles)
        self.assertEqual(self.employees, company.employees)

    def test_negative_mean_exception(self) -> None:
        with self.assertRaises(ValueError):
            Company(-1, self.std, self.quartiles, self.employees)

    def test_negative_std_exception(self) -> None:
        with self.assertRaises(ValueError):
            Company(self.mean, -1, self.quartiles, self.employees)

    def test_negative_quartiles_exception(self) -> None:
        with self.assertRaises(ValueError):
            Company(self.mean, self.std, (800, -1, 1200), self.employees)

    def test_impossible_std_exception(self) -> None:
        with self.assertRaises(ValueError):
            Company(self.mean, 2 * self.mean, self.quartiles, self.employees)

    def test_impossible_quartiles_exception(self) -> None:
        with self.assertRaises(ValueError):
            Company(-1, self.std, (1200, 1000, 800), self.employees)

    def test_propose_only_non_negative_salary(self) -> None:
        company = Company(self.mean, self.std, self.quartiles, self.employees)
        salaries = [company.get_salary() for _ in range(100)]
        self.assertGreaterEqual(min(salaries), 0)


if __name__ == '__main__':
    unittest.main()
