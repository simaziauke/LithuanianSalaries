import unittest
from random import randint
from lithuanian_salaries import Payslip


class PayslipTest(unittest.TestCase):
    def setUp(self) -> None:
        self.salaries = [randint(1000, 10000) for _ in range(100)]

    def test_salary_attribute(self) -> None:
        payslip = Payslip(self.salaries)
        self.assertListEqual(sorted(self.salaries), payslip.salaries)

    def test_negative_salary_exception(self) -> None:
        with self.assertRaises(ValueError):
            Payslip(self.salaries + [-1000])

    def test_mean(self) -> None:
        payslip = Payslip(self.salaries)
        self.assertAlmostEqual(sum(self.salaries) / len(self.salaries), payslip.mean)

    def test_constant_std(self) -> None:
        payslip = Payslip([randint(1000, 10000)] * 100)
        self.assertAlmostEqual(0, payslip.std)

    def test_str_line_count(self) -> None:
        payslip = Payslip(self.salaries)
        payslip_str_lines = str(payslip).splitlines()
        self.assertEqual(len(self.salaries), len(payslip_str_lines))

    def test_small_gross_salary_exception(self) -> None:
        with self.assertRaises(ValueError):
            Payslip(self.salaries + [1])

    def test_convert_gross_to_net(self) -> None:
        payslip = Payslip(self.salaries)
        self.assertTrue(payslip.taxes)
        payslip = payslip.to_net()
        self.assertFalse(payslip.taxes)

    def test_convert_net_to_net_exception(self) -> None:
        payslip = Payslip(self.salaries, taxes=False)
        with self.assertRaises(TypeError):
            _ = payslip.to_net()

    def test_convert_gross_to_net_reduces_amounts(self) -> None:
        payslip_gross = Payslip(self.salaries)
        payslip_net = payslip_gross.to_net()
        self.assertEqual(len(payslip_gross), len(payslip_net))
        for n in range(len(self.salaries)):
            self.assertGreater(payslip_gross.salaries[n], payslip_net.salaries[n])


if __name__ == '__main__':
    unittest.main()
