#!/usr/bin/env python
"""Command line interface to interact with Lithuanian Salaries tool."""
import click
from typing import Tuple
import lithuanian_salaries

Quartiles = Tuple[float, float, float]


@click.command()
@click.option('--mean', '-m', type=float, required=True,
              help='An average value of salaries.')
@click.option('--std', '-s', type=float, required=True,
              help='A standard deviation of salaries.')
@click.option('--quartiles', '-q', nargs=3, type=float,
              help='25%, 50%, 75% percentiles of salaries.')
@click.option('--employees', '-e', type=int, default=100, show_default=True,
              help='A number of employees.')
@click.option('--include-taxes', is_flag=True, default=False, show_default=True,
              help='Display gross salary values.')
@click.option('--best-of', type=int, default=100, show_default=True,
              help='Choose from multiple simulations.')
def display_salaries(mean: float, std: float, quartiles: Quartiles, employees: int,
                     include_taxes: bool, best_of: int) -> None:
    """Display a representative list of salaries given the summary statistics."""
    if not quartiles:
        quartiles = (mean - std, mean, mean + std)

    print('Company summary statistics:')
    print(f'Mean {mean:8.2f}')
    print(f'STD {std:9.2f}')
    print(f'Q1 {quartiles[0]:10.2f}')
    print(f'Q2 {quartiles[1]:10.2f}')
    print(f'Q3 {quartiles[2]:10.2f}')

    try:
        company = lithuanian_salaries.Company(mean, std, quartiles, employees)
    except ValueError as err:
        print(str(err))
        return

    optimizer = lithuanian_salaries.GeneticOptimizer(lambda x: abs(x.mean - company.mean) + abs(x.std - company.std))
    for _ in range(best_of):
        optimizer.push(company.get_payslip())
    optimizer.evolve()
    payslip = optimizer.pop()

    print(f"A possible list of ({'gross' if include_taxes else 'net'}) salaries:")
    print(payslip if include_taxes else payslip.to_net())

    print('Simulation statistics:')
    print(f'Mean {payslip.mean:8.2f} ({payslip.mean - mean:+.0f})')
    print(f'STD {payslip.std:9.2f} ({payslip.std - std:+.0f})')


if __name__ == '__main__':
    display_salaries()
