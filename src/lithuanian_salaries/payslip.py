"""Represent a collection of salaries as a payslip."""
from lithuanian_salaries import __minimal_wage__, Tax
import statistics
from typing import Iterator, List


class Payslip:
    """Entity that represents specific salaries.

    Attributes:
        salaries: A List of salaries in the payslip.
        taxes: Whether salaries include taxes (gross) or not (net).

    """

    def __init__(self, salaries: List[int], taxes: bool = True) -> None:
        """Initialize Payslip with specific salaries."""
        min_salary = min(salaries)
        if min_salary < __minimal_wage__:
            if min_salary < 0:
                raise ValueError(f'Cannot go below zero for salaries!')
            elif taxes:
                raise ValueError(f'Cannot go below the current minimal wage of {__minimal_wage__}!')
        self.salaries = sorted(salaries)
        self.taxes = taxes

    def __gt__(self, other: 'Payslip') -> bool:
        """Compare a payslip salary-by-salary."""
        return self.salaries > other.salaries

    def __iter__(self) -> Iterator[int]:
        """Iterate over the payslip salaries."""
        yield from self.salaries

    def __len__(self) -> int:
        """Return a number of salaries in a payslip."""
        return len(self.salaries)

    def __str__(self) -> str:
        """Visualize the payslip using charts."""
        label_size = len(str(self.salaries[-1]))
        labels = [f'[{str(s).rjust(label_size)}]' for s in self]

        chart_max_width = 50
        blocks = [' ', '▏', '▎', '▍', '▌', '▋', '▊', '▉', '█']
        block_max_value = self.salaries[-1] / chart_max_width
        block_min_value = block_max_value / (len(blocks) - 1)
        charts = [blocks[-1] * int(s / block_max_value) +
                  blocks[round((s % block_max_value) / block_min_value)] for s in self]

        return '\n'.join(map(' '.join, zip(labels, charts)))

    @property
    def mean(self) -> float:
        """Return an average value of the payslip."""
        return statistics.mean(self)

    @property
    def std(self) -> float:
        """Return a standard deviation of the payslip."""
        return statistics.stdev(self)

    def to_net(self) -> 'Payslip':
        """Return a payslip with deducted taxes."""
        if not self.taxes:
            raise TypeError('Cannot deduct taxes twice!')

        salaries_net = [s - int(Tax(s)) for s in self]
        return Payslip(salaries_net, taxes=False)
