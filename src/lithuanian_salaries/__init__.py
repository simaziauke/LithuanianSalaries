"""Global configurations and imports for Lithuanian Salaries tool."""
__version__ = '1.2.0'
__minimal_wage__ = 642  # 2021
__average_wage__ = 1517  # Q1 2021

from .tax import Tax
from .payslip import Payslip
from .company import Company
from .optimizer import GeneticOptimizer
from .cli import display_salaries

__all__ = [Tax, Payslip, Company, GeneticOptimizer, display_salaries]
