"""Generate, describe and evaluate salary tendencies within a company."""
from lithuanian_salaries import __minimal_wage__, Payslip
import math
import numpy as np
from random import random
import sys
from typing import Tuple

Quartiles = Tuple[float, float, float]


class Company:
    """Entity that works with summary statistics.

    Attributes:
        mean: An average value of salaries.
        std: A standard deviation of salaries.
        quartiles: 25%, 50%, 75% percentiles of salaries.
        employees: A number of employees.
        _shape: A shape parameter for Pareto salary distribution.
        _scale: A scale parameter for Pareto salary distribution.

    """

    def __init__(self, mean: float, std: float, quartiles: Quartiles, employees: int) -> None:
        """Initialize Company with publicly available summary statistics."""
        if min(quartiles + (mean,)) < __minimal_wage__:
            raise ValueError(f'Cannot go below the current minimal wage of {__minimal_wage__}!')
        if not 0 <= std < mean:
            raise ValueError('Impossible standard deviation!')
        if not quartiles[0] <= quartiles[1] <= quartiles[2]:
            raise ValueError('Impossible quartiles!')
        if employees < 5:
            if employees < 1:
                raise ValueError('Negative number of employees!')
            raise ValueError('Trivial number of employees!')

        self.mean = mean
        self.std = std
        self.quartiles = quartiles
        self.employees = employees

        self._shape = 1 + math.sqrt(1 + (mean / std) ** 2)
        self._scale = mean * (1 - 1 / self._shape)

    def get_salary(self) -> int:
        """Generate a random salary from the derived symmetrical Pareto distribution."""
        factor = 1 + (1 if random() < 0.5 else -1) * np.random.pareto(self._shape)
        return int(self._scale * abs(factor))

    def get_payslip(self) -> Payslip:
        """Create a possible company payslip from generated salaries."""
        salaries = []
        limits = [__minimal_wage__, *self.quartiles, sys.maxsize]
        for n in range(1, len(limits)):
            while len(salaries) < n * (self.employees / 4 - 1):
                salary = self.get_salary()
                if limits[n - 1] <= salary <= limits[n]:
                    salaries.append(salary)

        return Payslip(salaries + [int(s) for s in self.quartiles])
