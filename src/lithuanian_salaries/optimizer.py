"""A context-specific optimization engine."""
import heapq
from secrets import randbelow, randbits
from typing import Any, Callable


class GeneticOptimizer:
    """Genetic optimizer with crossover and no mutations.

    Attributes:
        fit: A function that evaluates the fitness of a member.
        _population: A collection of (fitness, member) tuples.

    """

    def __init__(self, fit: Callable[[Any], float]) -> None:
        """Initialize a GeneticOptimizer with a fitness function."""
        self.fit = fit
        self._population = []

    def __len__(self) -> int:
        """Return the current size of a population."""
        return len(self._population)

    @property
    def fitness(self) -> float:
        """Return a fitness value of the best-performing member."""
        return self._population[0][0]

    def push(self, member: Any) -> None:
        """Insert a new member into the population."""
        heapq.heappush(self._population, (self.fit(member), member))

    def pop(self) -> Any:
        """Get the top member from the population."""
        return heapq.heappop(self._population)[1]

    def evolve(self, target: float = None) -> None:
        """Mix population with its best offsprings until no improvement is found."""
        parents = [parent for _, parent in self._population]
        for n in range(1, len(parents)):
            parent = parents[n]
            other_parent = parents[randbelow(n)]
            offspring = type(parent)([x[randbits(1)] for x in zip(parent, other_parent)])
            self.push(offspring)
        self._population = self._population[:len(parents)]

        if target is None or self.fitness < target:
            self.evolve(self.fitness)
