"""Applications of tax-related mechanics."""
from lithuanian_salaries import __average_wage__, __minimal_wage__


class Tax:
    """Entity that calculates taxes for a given amount of money earned on a job contract.

    Attributes:
        exempt: Amount excluded from the taxation of income.
        income_default: Standard income taxes.
        income_premium: Premium income taxes (if any).
        social_health: Health insurance taxes.
        social_pension: Pension fund taxes.

    """

    _rate_income_default = 0.2
    # Reduce the taxable income for low salaries
    _limit_exempt = 400
    _rate_exempt_deterioration = 0.18
    # Use a higher tax rate for high salaries
    _rate_income_premium = 0.32
    _threshold_income_premium = (60 * __average_wage__) // 12  # per month

    _rate_social_health = 0.0698
    _rate_social_pension = 0.1252

    def __init__(self, amount: int) -> None:
        """Initialize Tax with an amount of money."""
        if amount < 0:
            raise ValueError('Error: a negative amount of money!')

        exempt_potential = Tax._limit_exempt - Tax._rate_exempt_deterioration * (amount - __minimal_wage__)
        self.exempt = max(0.0, min(Tax._limit_exempt, exempt_potential))

        self.income_default = max(0.0, Tax._rate_income_default * (amount - self.exempt))
        self.income_premium = max(0.0, (Tax._rate_income_premium - Tax._rate_income_default) *
                                  (amount - Tax._threshold_income_premium))

        self.social_health = Tax._rate_social_health * amount
        self.social_pension = Tax._rate_social_pension * amount

    def __int__(self) -> int:
        """Accumulate all calculated taxes into a single sum."""
        return int(self.income_default + self.income_premium + self.social_health + self.social_pension)
